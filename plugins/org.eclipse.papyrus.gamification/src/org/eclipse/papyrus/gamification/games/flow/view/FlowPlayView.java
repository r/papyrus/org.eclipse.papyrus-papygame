/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.games.flow.view;


import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.listener.NotificationListener;
import org.eclipse.papyrus.gamification.data.entity.UmlDiagramSolution;
import org.eclipse.papyrus.gamification.data.jsonmapper.UMLToJSONMapper;
import org.eclipse.papyrus.gamification.data.jsonmapper.UmlClassDiagram;
import org.eclipse.papyrus.gamification.games.flow.JSHintRequest;
import org.eclipse.papyrus.gamification.games.flow.JSHintRequest.HintInterface;
import org.eclipse.papyrus.gamification.games.framework.LevelExecutor;
import org.eclipse.papyrus.gamification.games.framework.communication.OnGameEndedItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnModelChangedItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnPlayerAskHint;
import org.eclipse.papyrus.gamification.games.framework.communication.OnResumeToDashboardItf;
import org.eclipse.papyrus.gamification.games.framework.entity.LevelContext;
import org.eclipse.papyrus.gamification.games.framework.entity.PapyrusContext;
import org.eclipse.papyrus.gamification.games.oyo.JSTestMyProposition;
import org.eclipse.papyrus.gamification.games.oyo.JSTestMyProposition.TestMyPropositionInterface;
import org.eclipse.papyrus.gamification.modelutils.ModelDisplayManager;
import org.eclipse.papyrus.gamification.modelutils.papyrus.PapyrusAttribute;
import org.eclipse.papyrus.gamification.modelutils.papyrus.PapyrusClass;
import org.eclipse.papyrus.gamification.modelutils.papyrus.PapyrusClassDiagram;
import org.eclipse.papyrus.gamification.modelutils.papyrus.PapyrusNode;
import org.eclipse.papyrus.gamification.modelutils.papyrus.PapyrusOperation;
import org.eclipse.papyrus.gamification.modelutils.papyrus.copy.Mask;
import org.eclipse.papyrus.gamification.view.common.swt.BrowserWrapper;
import org.eclipse.papyrus.gamification.view.game.GamePlayView;
import org.eclipse.papyrus.gamification.view.game.JSResumeToDashboard;
import org.eclipse.papyrus.uml.diagram.common.editparts.ClassEditPart;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;

/**
 * @author zaza Le Pallec extended by antbucc
 */


public class FlowPlayView extends GamePlayView implements NotificationListener, TestMyPropositionInterface, HintInterface, OnResumeToDashboardItf,OnModelChangedItf{

	private int propositionCounter = 0;
	UmlClassDiagram userUmlClassDiagram;
	private OnResumeToDashboardItf onResumeToDashboardItf;

	/**
	 * Constructor.
	 *
	 * @param levelContext
	 * @param papyrusContext
	 * @param onGameEndedItf
	 */
	public FlowPlayView(LevelContext levelContext, PapyrusContext papyrusContext, OnGameEndedItf onGameEndedItf, OnPlayerAskHint onPlayerAskHint, Boolean hintAsked, OnResumeToDashboardItf onResumeToDashboardItf, OnModelChangedItf onModelChangedItf) {
		super(levelContext, papyrusContext, onModelChangedItf, onGameEndedItf,onPlayerAskHint,hintAsked,onResumeToDashboardItf);
	}


	@Override
	public void registerJavaScriptFunctions(BrowserWrapper browser) {
		super.registerJavaScriptFunctions(browser);
		new JSTestMyProposition(browser, this);
		new JSHintRequest(browser,this);
		new JSResumeToDashboard(browser, this);
	}

	@Override
	public String getHtmlPath() {
		return "/html/games/flow/html/game.html"; //$NON-NLS-1$
	}

	public void onGameEnded(boolean success, int goodMoves, int badMoves) {

		/*
		 * ActionRequestContent actionRequestContent = new ActionRequestContent("taskCompleted");
		 * actionRequestContent.setGameId(gameContext.getSeries().getSeriesGameId());
		 * actionRequestContent.setPlayerId(gameContext.getPlayerProfile().getPlayerId());
		 * ActionRequestCustomData arcd = new ActionRequestCustomData(goodMoves + badMoves, badMoves, gameContext.getLevel().getLabel());
		 * actionRequestContent.setData(arcd);
		 *
		 * levelPresenter.submitGame(actionRequestContent);
		 */

	}

	/**
	 * @see org.eclipse.papyrus.gamification.view.common.DisplayableView#onHtmlPageLoaded(org.eclipse.swt.browser.Browser)
	 *
	 * @param browser
	 */
	@Override
	public void onHtmlPageLoaded(BrowserWrapper browser) {
		super.onHtmlPageLoaded(browser);
	}


	public void userAsksToTest(Boolean withCheck) {

		ModelDisplayManager.saveEditor();
		PapyrusClassDiagram playerClassDiagram = papyrusContext.getPlayerDiagram();
		// PapyrusClassDiagram originalClassDiagram = papyrusContext.getOriginalDiagram();

		// Should have one class to send
		if(playerClassDiagram.getClasses().length > 0) {
			PapyrusClass pc = playerClassDiagram.getClasses()[0];
			Model m = ((ClassEditPart) pc.getEditPart()).getUMLElement().getModel();
			userUmlClassDiagram = UMLToJSONMapper.map(m);
			getSpentTime();
		}
		else {
			//System.out.println("No classes modeled - TBD an alert to notify the Student");
		}
	}







	/**
	 * @see org.eclipse.papyrus.gamification.games.flow.JSHintRequest.AskHintInterface#userAsksHint
	 *
	 * @param ??
	 */
	@Override
	public void userAsksHint() {
		askHint();

	}




	/**
	 * @see org.eclipse.papyrus.gamification.view.game.GamePlayView#proceedWithTime(int)
	 *
	 * @param seconds
	 */
	@Override
	public void proceedWithTime(Double seconds) {
		UmlDiagramSolution umlDiagramSolution = new UmlDiagramSolution();

		umlDiagramSolution.setUmlClassDiagram(userUmlClassDiagram);
		umlDiagramSolution.setTimeSpent(seconds.intValue());



		endGame(umlDiagramSolution);
	}


	@Override
	public void onResumeToDashboard() {
		LevelExecutor.getInstance().onResumeToDashboard();
	}




	@Override
	public void onModelChanged(Notification notification) {
		if (notification.getEventType() == Notification.ADD
				|| notification.getEventType() == Notification.ADD_MANY) {
			if (notification.getNewValue() instanceof EObject) {
				EObject newObject = EObject.class.cast(notification.getNewValue());
				PapyrusNode addedPapyrusNode = papyrusContext.getPlayerDiagram().getCorrespondingPapyrusNode(newObject);
				if (addedPapyrusNode instanceof PapyrusAttribute) {
					PapyrusAttribute playerAttribute = PapyrusAttribute.class.cast(addedPapyrusNode);
					PapyrusAttribute originalAttribute = PapyrusAttribute.class.cast(papyrusContext.getDiagramClassCopier().getCorrespondingOriginalElement(playerAttribute));

					String playerContainerName = NamedElement.class.cast(playerAttribute.getSemanticElement().eContainer()).getName();
					String originalContainerName = NamedElement.class.cast(originalAttribute.getSemanticElement().eContainer()).getName();

					playerContainerName = new Mask(playerContainerName).getFinalName();
					originalContainerName = new Mask(originalContainerName).getFinalName();

					/*if (!playerContainerName.equals(originalContainerName)) {
						handleBadMove(playerAttribute.getName());
						// this.papyrusContext.getTransactionalEditingDomain().getCommandStack().undo();
					} else {
						handleGoodMove(playerAttribute.getName());
					}*/
				} else if (addedPapyrusNode instanceof PapyrusOperation) {
					PapyrusOperation playerOperation = PapyrusOperation.class.cast(addedPapyrusNode);
					PapyrusOperation originalOperation = PapyrusOperation.class.cast(papyrusContext.getDiagramClassCopier().getCorrespondingOriginalElement(playerOperation));

					String playerContainerName = NamedElement.class.cast(playerOperation.getSemanticElement().eContainer()).getName();
					String originalContainerName = NamedElement.class.cast(originalOperation.getSemanticElement().eContainer()).getName();

					playerContainerName = new Mask(playerContainerName).getFinalName();
					originalContainerName = new Mask(originalContainerName).getFinalName();

					/*if (!playerContainerName.equals(originalContainerName)) {
						handleBadMove(playerOperation.getName());
						// this.papyrusContext.getTransactionalEditingDomain().getCommandStack().undo();
					} else {
						handleGoodMove(playerOperation.getName());
					}*/
				}


			}
		}
	}


	@Override
	public void notifyChanged(Notification notification) {
		// Implementa il tuo codice qui per gestire il cambiamento della notifica




	}
}

