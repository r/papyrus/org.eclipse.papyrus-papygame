/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.games.flow;


import org.eclipse.papyrus.gamification.view.common.swt.BrowserWrapper;
import org.eclipse.papyrus.gamification.view.common.swt.JavascriptFunction;

/**
 * @author antbucc
 *
 */
public class JSHintRequest extends JavascriptFunction {

	BrowserWrapper browser;
	HintInterface askHint;

	public interface HintInterface {
		public void userAsksHint();
	}



	public JSHintRequest(BrowserWrapper browser, HintInterface askHint) {
		super(browser, "hintRequest");
		this.browser = browser;
		this.askHint = askHint;
	}

	@Override
	public Object functionBody(Object[] args) {
		try {
			askHint.userAsksHint();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "";
	}

}
