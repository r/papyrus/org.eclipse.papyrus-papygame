/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.view.game;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.gamification.data.entity.GameMetrics;
import org.eclipse.papyrus.gamification.data.entity.UmlDiagramSolution;
import org.eclipse.papyrus.gamification.games.framework.communication.OnGameEndedItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnModelChangedItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnPlayerAskHint;
import org.eclipse.papyrus.gamification.games.framework.communication.OnResumeToDashboardItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnSecondsReadyItf;
import org.eclipse.papyrus.gamification.games.framework.entity.LevelContext;
import org.eclipse.papyrus.gamification.games.framework.entity.PapyrusContext;
import org.eclipse.papyrus.gamification.modelutils.ModelChangesListenerAdapter;
import org.eclipse.papyrus.gamification.modelutils.papyrus.PapyrusClassDiagram;
import org.eclipse.papyrus.gamification.view.common.swt.BrowserWrapper;
import org.eclipse.papyrus.gamification.view.common.swt.JSOnSecondsReady;
import org.eclipse.uml2.uml.Model;

import com.google.gson.Gson;

/**
 * @author maximesavaryleblanc
 *
 */
public abstract class GamePlayView extends GameView implements OnModelChangedItf, OnSecondsReadyItf {

	private OnGameEndedItf onGameEndedItf;
	private OnPlayerAskHint onPlayerAskHint;
	private OnResumeToDashboardItf onResumeToDashboardItf;
	private OnModelChangedItf onModelChangedItf;
	private Boolean hintAsked;
	protected LevelContext levelContext;
	protected PapyrusContext papyrusContext;
	protected BrowserWrapper browser;

	public GamePlayView(LevelContext levelContext, PapyrusContext papyrusContext, OnModelChangedItf onModelChangedItf, OnGameEndedItf onGameEndedItf, OnPlayerAskHint onPlayerAskHint,Boolean hintAsked,OnResumeToDashboardItf onResumeToDashboardItf) {
		this.papyrusContext = papyrusContext;
		this.levelContext = levelContext;
		this.onPlayerAskHint = onPlayerAskHint;
		this.hintAsked = hintAsked;
		this.onGameEndedItf = onGameEndedItf;
		this.onResumeToDashboardItf = onResumeToDashboardItf;
		this.onModelChangedItf = onModelChangedItf;

	}


	@Override
	public void registerJavaScriptFunctions(BrowserWrapper browser) {

		super.registerJavaScriptFunctions(browser);
		new JSOnSecondsReady(browser, this);
		this.browser = browser;
	}


	@Override
	public void start() {
		super.start();


		listenActionsOnModel();
	}


	protected void askHint() {



		int goldcoins = this.levelContext.getPlayerProfile().getGoldCoins();
		if(goldcoins >= 10)
		{
			// the hint can be used
			//callJSScript("waitForResults()");
			onPlayerAskHint.onPlayerAskHint();
			String levelDescription = levelContext.getLevel().getStatement().replace("'", "\'");
			String hint = levelContext.getLevel().getHint().replace("'", "\'");
			String result = levelDescription +"<br/> <b><h3 style=\"color:Green;\"> "+hint+"</h3> <b/> ";
			callJSScript("setLevelStatement(`" + result + "`)");
		}

		else {
			// the hint can not be used		
			String levelDescription = levelContext.getLevel().getStatement().replace("'", "\'");
			String result = levelDescription +"<br/> <b><h3 style=\"color:Red;\">Gold Coins not enough to get the Hint</h3> <b/> <br/> ";
			callJSScript("setLevelStatement(`" + result + "`)");

		}

	}



	protected void endGame(GameMetrics gameMetrics) {
		callJSScript("waitForResults()");
		onGameEndedItf.onGameEnded(gameMetrics);
	}

	protected void endGame(UmlDiagramSolution umlDiagramSolution) {
		callJSScript("waitForResults()");
		onGameEndedItf.onGameEnded(umlDiagramSolution);
	}

	/* endGame for activity without model evaluation */
	protected void endGame(Boolean result) {
		callJSScript("waitForResults()");
		onGameEndedItf.onGameEnded(result);
	}






	private void listenActionsOnModel() {
		Model modelToListen = null;
		PapyrusClassDiagram diagram = this.papyrusContext.getPlayerDiagram();
		if (this.papyrusContext == null) {
			// case of the FLOW game type - no diagram to compare/evaluate
			return;
		} 
		/*	else if (diagram.allNodes().size() == 0) {
			modelToListen = papyrusContext.getPlayerModel().getModel();
			modelToListen.eAdapters().add(new ModelChangesListenerAdapter(this));



			return;
		}*/
		else {

			EObject firstValue = papyrusContext
					.getPlayerDiagram().allNodes().iterator().next().getSemanticElement();
			while (firstValue != null) {
				if (firstValue instanceof Model) {
					modelToListen = Model.class.cast(firstValue);
				}
				firstValue = firstValue.eContainer();
			}

			modelToListen.eAdapters().add(new ModelChangesListenerAdapter(this));
		}
	}


	@Override
	public void onHtmlPageLoaded(BrowserWrapper browser) {
		super.onHtmlPageLoaded(browser);
		if(!this.hintAsked) {
			callJSScript("setLevelStatement(`" + levelContext.getLevel().getStatement().replace("'", "\'") + "`)");
		}
		else {
			// here we add also the Hint
			String levelDescription = levelContext.getLevel().getStatement().replace("'", "\'");
			String hintDescription = levelContext.getLevel().getHint().replace("'", "\'");
			String result = levelDescription +"<br/> <b><h3  style=\"color:Green;\">Hint</h3> <b/> <br/> "+hintDescription;
			callJSScript("setLevelStatement(`" + result + "`)");
		}

	}


	@Override
	public void onModelChanged(Notification notification) {
		// Do nothing
	}

	protected void getSpentTime() {
		callJSScript("getSecondsSpent();");
	}

	public abstract void proceedWithTime(Double seconds);

	/**
	 * @see org.eclipse.papyrus.gamification.games.framework.communication.OnSecondsReadyItf#onSecondsReady(int)
	 *
	 * @param seconds
	 */
	@Override
	public void onSecondsReady(Double seconds) {
		// TODO Auto-generated method stub
		proceedWithTime(seconds);
	}
}
