/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.view.game;

import org.eclipse.papyrus.gamification.games.framework.communication.OnCancelGameItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnPlayerAnswerItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnPlayerReadyItf;
import org.eclipse.papyrus.gamification.games.framework.entity.LevelContext;
import org.eclipse.papyrus.gamification.view.common.swt.BrowserWrapper;
import org.eclipse.swt.widgets.Display;

/**
 * @author maximesavaryleblanc
 *
 */
public class GameQuizView extends GameView implements OnPlayerAnswerItf, OnCancelGameItf{

	LevelContext levelContext;
	OnPlayerAnswerItf onPlayerAnswerItf;
	OnCancelGameItf onCancelGameItf;

	public GameQuizView(LevelContext levelContext, OnPlayerAnswerItf onPlayerAnswerItf, OnCancelGameItf onCancelGameItf) {
		super();
		this.levelContext = levelContext;
		this.onPlayerAnswerItf = onPlayerAnswerItf;
		this.onCancelGameItf = onCancelGameItf;
	}

	@Override
	public void registerJavaScriptFunctions(BrowserWrapper browser) {
		super.registerJavaScriptFunctions(browser);
		new JSPlayerAnswer(browser, onPlayerAnswerItf);
		new JSCancelGame(browser, onCancelGameItf);
	}

	@Override
	public void clearJavascriptFunctions(BrowserWrapper browser) {
		// TODO Auto-generated method stub
		super.clearJavascriptFunctions(browser);
	}

	@Override
	public void onHtmlPageLoaded(BrowserWrapper browser) {
		super.onHtmlPageLoaded(browser);
		callJSScript("activeCancelButton()");
		callJSScript("setLevelLabel('" + levelContext.getLevel().getLabel().replace("'", "\'") + "')");
		callJSScript("setSeriesLabel('" + levelContext.getSeries().getName().replace("'", "\'") + "')");
		callJSScript("setLevelStatement(`" + levelContext.getLevel().getStatement().replace("'", "\'") + "`)");
	    callJSScript("setAnswer1(`" + levelContext.getLevel().getAnswer1().replace("'", "\'") + "`)");
	    callJSScript("setAnswer2(`" + levelContext.getLevel().getAnswer2().replace("'", "\'") + "`)");
	    callJSScript("setAnswer3(`" + levelContext.getLevel().getAnswer3().replace("'", "\'") + "`)");
	    callJSScript("setAnswer4(`" + levelContext.getLevel().getAnswer4().replace("'", "\'") + "`)");

	}


	@Override
	public void onVideoFinished() {
		callJSScript("activeReadyButton()");
	}



	@Override
	public void onCancelGame() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				onCancelGameItf.onCancelGame();
			}
		});
	}

	@Override
	public void onPlayerAnswer(String value) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				onPlayerAnswerItf.onPlayerAnswer(value);
			}
		});
	}
	
	
}
