/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.view.game;

import org.eclipse.papyrus.gamification.games.framework.communication.OnPlayerAnswerItf;
import org.eclipse.papyrus.gamification.games.framework.communication.OnPlayerReadyItf;
import org.eclipse.papyrus.gamification.view.common.swt.BrowserWrapper;
import org.eclipse.papyrus.gamification.view.common.swt.JavascriptFunction;


public class JSPlayerAnswer extends JavascriptFunction {

	BrowserWrapper browser;
	OnPlayerAnswerItf playerAnswerItf;

	public JSPlayerAnswer(BrowserWrapper browser, OnPlayerAnswerItf playerAnswerItf) {
		super(browser, "playerAnswer");
		this.browser = browser;
		this.playerAnswerItf = playerAnswerItf;
	}

	@Override
	public Object functionBody(Object[] args) {
		try {
			playerAnswerItf.onPlayerAnswer(String.class.cast(args[0]));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "";
	}
	

}
