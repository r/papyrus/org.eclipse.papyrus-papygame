/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.data.repository.level;

import org.eclipse.papyrus.gamification.Preferences;
import org.eclipse.papyrus.gamification.data.api.GameManagerService;
import org.eclipse.papyrus.gamification.data.api.query.ActionRequestContent;
import org.eclipse.papyrus.gamification.data.api.query.ActionRequestCustomData;
import org.eclipse.papyrus.gamification.data.api.response.CustomDataJson;
import org.eclipse.papyrus.gamification.data.api.response.PlayerStatusJson;
import org.eclipse.papyrus.gamification.data.entity.GameMetrics;
import org.eclipse.papyrus.gamification.data.entity.UmlDiagramSolution;
import org.eclipse.papyrus.gamification.games.framework.entity.LevelContext;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author maximesavaryleblanc
 *
 */
public class LevelRemoteDataSource {

	GameManagerService gameManagerService;

	public LevelRemoteDataSource(GameManagerService gameManagerService) {
		this.gameManagerService = gameManagerService;
	}

	Completable submitGameResult(GameMetrics gameMetrics, LevelContext levelContext) {

		ActionRequestContent<ActionRequestCustomData> actionRequestContent = new ActionRequestContent<>(Preferences.SUBMIT_GAME_ACTION_ID);
		actionRequestContent.setPlayerId(levelContext.getPlayerProfile().getPlayerId());
		actionRequestContent.setGameId(levelContext.getSeries().getSeriesGameId());
		// actionRequestContent.setExecutionMoment(executionMoment);

		ActionRequestCustomData arcd = new ActionRequestCustomData();
		arcd.setMoves(gameMetrics.getOkMoves());
		arcd.setErrors(gameMetrics.getErrors());
		arcd.setLevelLabel(levelContext.getLevel().getLabel());
		arcd.setLogs("LOGS");
		arcd.setTimeSpent(gameMetrics.getTimeSpent());

		actionRequestContent.setData(arcd);

		return gameManagerService.submitGameResult(actionRequestContent.getGameId(), actionRequestContent.getActionId(), actionRequestContent);
	}

	//case where we do not need the model checking in the gamification engine  - Case of "Study" Activities for example
	// when is true we have success when is false is not a success - case of the Quiz activity
	Completable submitGameResult(LevelContext levelContext, Boolean result) {

		String actionType = "";
		if(levelContext.getLevel().getLabel().equalsIgnoreCase("QUIZ") && result)
		{
			actionType = Preferences.SUBMIT_GAME_ACTION_OK_QUIZ;
		}
		else if(levelContext.getLevel().getLabel().equalsIgnoreCase("QUIZ") && !result) {
			// case we fail - Quiz with failure
			actionType = Preferences.SUBMIT_GAME_ACTION_FAILED_QUIZ;
		}
		else if(levelContext.getLevel().getLabel().equalsIgnoreCase("STUDY")) {
			actionType = Preferences.SUBMIT_GAME_ACTION_NO_CHECK;
		}
		else if(levelContext.getLevel().getLabel().equalsIgnoreCase("PRACTICE")){
			// case where we use the hint and we need to decrease the Gold Coins
			actionType = Preferences.SUBMIT_GAME_ACTION_USE_HINT;
		}
		else if(levelContext.getLevel().getLabel().equalsIgnoreCase("EXTRA QUESTION") && !result) {
			// case we fail - Extra Question with failure
			actionType = Preferences.SUBMIT_GAME_ACTION_FAILED_EXTRA;
		}
		else if(levelContext.getLevel().getLabel().equalsIgnoreCase("EXTRA QUESTION") && result) {
			actionType = Preferences.SUBMIT_GAME_ACTION_OK_EXTRA;
		}
		else if(levelContext.getLevel().getLabel().equalsIgnoreCase("GREEN BELT") && result) {
			actionType = Preferences.SUBMIT_GAME_ACTION_GREEN_BELT;
		}




		ActionRequestContent<ActionRequestCustomData> actionRequestContent = new ActionRequestContent<>(actionType);
		actionRequestContent.setPlayerId(levelContext.getPlayerProfile().getPlayerId());
		actionRequestContent.setGameId(levelContext.getSeries().getSeriesGameId());
		// actionRequestContent.setExecutionMoment(executionMoment);

		ActionRequestCustomData arcd = new ActionRequestCustomData();
		arcd.setMoves(0);
		arcd.setErrors(0);
		arcd.setLevelLabel(levelContext.getLevel().getLabel());
		arcd.setLogs("LOGS");
		arcd.setTimeSpent(0);

		actionRequestContent.setData(arcd);


		// here we call the POST API of the Gamification Engine
		return gameManagerService.submitGameResult(actionRequestContent.getGameId(), actionRequestContent.getActionId(), actionRequestContent);
	}

	Completable submitGameResult(UmlDiagramSolution umlDiagramSolution, LevelContext levelContext) {

		ActionRequestContent<UmlDiagramSolution> actionRequestContent = new ActionRequestContent<>(Preferences.SUBMIT_GAME_ACTION_ID);
		actionRequestContent.setPlayerId(levelContext.getPlayerProfile().getPlayerId());
		actionRequestContent.setGameId(levelContext.getSeries().getSeriesGameId());
		// actionRequestContent.setExecutionMoment(executionMoment);
		actionRequestContent.setData(umlDiagramSolution);


		return gameManagerService.submitGameResult(actionRequestContent.getGameId(), actionRequestContent.getActionId(), actionRequestContent);
	}

	Single<PlayerStatusJson> getPlayerStatus(String gameId, String playerId) {


		return gameManagerService.getPlayerStatus(gameId, playerId);


	}

	Completable setPlayerCustomData(String gameId, String playerId, CustomDataJson content) {
		return gameManagerService.setPlayerCustomData(gameId, playerId, content);
	}

}
