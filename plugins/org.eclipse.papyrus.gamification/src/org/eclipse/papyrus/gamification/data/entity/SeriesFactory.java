/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.gamification.data.entity;

import java.util.List;

import com.google.gson.Gson;

/**
 * @author lepallec
 *
 */
public class SeriesFactory {

	static final String HANGMAN_CLASSNAME = "org.eclipse.papyrus.gamification.games.hangman.Hangman";
	static final String OYO_CLASSNAME = "org.eclipse.papyrus.gamification.games.oyo.Oyo";

	public static Series createBachelorSeries() {
		Series series = new Series("Bachelor of Software Engineering", "5f087a49857aba00013178cd");

		List<Level> levels = series.getLevels();
		return series;
	}

	public static Series createMasterSeries() {
		Series series = new Series("Master of Software Engineering", "5f087a57857aba00013178ce");

		List<Level> levels = series.getLevels();
		return series;
	}

}
