package org.eclipse.papyrus.gamification;

public class Preferences {
	//public static final boolean IS_MOCK = true;

	public static final String BASE_URL = "https://gamification-test.platform.smartcommunitylab.it";
	public static final String AUTH_USERNAME = "ge-test-admin";
	public static final String AUTH_PASSWORD = "7Nd31B5MzGWUZmYhUbtQ";

	public static final String INTRO_VIDEO_URL = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
	public static final String INTRO_VIDEO_PLACEHOLDER = "https://geo.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fgeo.2F2019.2F10.2F11.2Fc2d7f3c4-4905-4665-aae4-8d86ea1cb8c7.2Ejpeg/640x480/cr/wqkgUGl4YWJheSAvIEdFTw%3D%3D/picture.jpg";

	public static final String SUBMIT_GAME_ACTION_ID = "taskCompleted";
	public static final String SUBMIT_GAME_ACTION_NO_CHECK = "noCheck";
	public static final String SUBMIT_GAME_ACTION_OK_QUIZ = "okQuiz";
	public static final String SUBMIT_GAME_ACTION_FAILED_QUIZ = "failedQuiz";
	public static final String SUBMIT_GAME_ACTION_OK_EXTRA = "okExtra";
	public static final String SUBMIT_GAME_ACTION_FAILED_EXTRA = "failedExtra";
	public static final String SUBMIT_GAME_ACTION_USE_HINT = "useHint";
	public static final String SPONSOR_ACTION_ID = "playerInvited";
	public static final String SUBMIT_GAME_ACTION_GREEN_BELT = "greenBelt";

	public static final String PAPY_QUESTIONNAIRE_URL = "https://www.papygame.com/questionnarie";
	public static final String PAPY_PRIZE_URL = "https://www.papygame.com/prize/";

	public static final String PAPYRUS_MODELS_ID = "5f885af1857aba00013f54e8";



	public static final String VERSION = "2.0.0";

	public static final String RESPONSE_STORAGE = "/org.eclipse.papyrus.gamification";

}
